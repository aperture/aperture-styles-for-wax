// add button to show hide structure

const styles = `position: fixed; top: 0; right: 0; padding: 1em; z-index: 10000`

let tools = document.createElement("div");
tools.classList.add("tools");
tools.style = styles;

document.querySelector('.ProseMirror').insertAdjacentElement("afterbegin", tools);

let button = document.createElement("button");
button.innerHTML = "show/hide structure";
button.style = styles;

createButton("show structure", 'showStruct');
createButton("force show", 'forceShow');

// button.addEventListener('click', function() {
//     document.querySelector('.ProseMirror').classList.toggle('showStruct');
// })


function createButton(word, actionClass) {
    let button = document.createElement("button");
    button.innerHTML = word;
    button.addEventListener('click', function () {
        document.querySelector('.ProseMirror').classList.toggle(actionClass);
    })
    document.querySelector('.tools').insertAdjacentElement("beforeend", button);
}
